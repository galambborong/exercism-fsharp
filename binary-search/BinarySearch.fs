﻿module BinarySearch

open System

let rec go (input: int array) target startI endI =
    let middleI =
        Math.Floor((startI + endI) / 2 |> float) |> int

    let whenTargetGreater =
        let newStartI = middleI + 1

        match newStartI <= endI with
        | true -> go input target newStartI endI
        | false -> None

    let whenTargetLess =
        let newEndI = middleI - 1

        match newEndI >= startI with
        | true -> go input target startI (middleI - 1)
        | false -> None

    match target with
    | t when t > input[middleI] -> whenTargetGreater
    | t when t < input[middleI] -> whenTargetLess
    | t when t = input[middleI] -> Some middleI
    | _ -> None

let find input value =
    let len = input |> Array.length

    match len with
    | 0 -> None
    | _ -> go input value 0 (len - 1)
